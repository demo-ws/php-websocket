<?php

declare(strict_types=1);

namespace SocketIO\Server\Connection;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use SocketIO\Server\Connection\State\SateFactoryInterface;
use SocketIO\Server\Server;

class ConnectionFactory implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /** @var LoggerInterface */
    protected $logger;
    private SateFactoryInterface $stateFactory;

    public function __construct(SateFactoryInterface $stateFactory)
    {
        $this->stateFactory = $stateFactory;
        $this->logger = new NullLogger();
    }

    /**
     * @param resource $socket
     * @param Server $server
     *
     * @return Connection
     */
    public function getConnection($socket, Server $server): Connection
    {
        $connection = new Connection($this->stateFactory, $server, $socket);
        $connection->setLogger($this->logger);
        return $connection;
    }
}
