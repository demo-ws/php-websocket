<?php

declare(strict_types=1);

namespace Test\SocketIO;

use PHPUnit\Framework\TestCase;

class Test extends TestCase
{
    /** @var resource */
    private $inStream;
    /** @var resource */
    private $outStream;

    protected function setUp(): void
    {
        parent::setUp();

        [$this->inStream, $this->outStream] = stream_socket_pair(
            STREAM_PF_UNIX,
            STREAM_SOCK_STREAM,
            STREAM_IPPROTO_IP
        );
    }

    protected function tearDown(): void
    {
        fclose($this->inStream);
        fclose($this->outStream);
    }

    public function testCreate()
    {
        $socket = stream_socket_server(sprintf(
            'tcp://%s:%d',
            '127.0.0.1',
            3534
        ), $errorCode, $errorMessage, STREAM_SERVER_BIND | STREAM_SERVER_LISTEN);

        self::assertEquals(0, $errorCode);
        self::assertEquals('', $errorMessage);

        stream_set_blocking($socket, false);

        $client = stream_socket_client('tcp://127.0.0.1:3534', $errorCode, $errorMessage, 1, STREAM_CLIENT_CONNECT);
        $connect = @stream_socket_accept($socket, 1);
        stream_set_timeout($connect, 0, 100);
        stream_set_timeout($client, 0, 100);
        stream_set_blocking($connect, false);
        stream_set_blocking($client, false);


        self::assertFalse(feof($connect));
        fclose($client);
        self::assertTrue(feof($connect));
    }
}
