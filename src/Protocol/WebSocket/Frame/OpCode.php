<?php

declare(strict_types=1);

namespace SocketIO\Protocol\WebSocket\Frame;

/**
 * @method static self TEXT()
 * @method static self DATA()
 * @method static self USR1()
 * @method static self USR2()
 * @method static self USR3()
 * @method static self USR4()
 * @method static self USR5()
 *
 * @method static self CLOSE()
 * @method static self PING()
 * @method static self PONG()
 * @method static self CTRL1()
 * @method static self CTRL2()
 * @method static self CTRL3()
 * @method static self CTRL4()
 * @method static self CTRL5()
 */
class OpCode
{
    public const OPCODE_TEXT = 0x01;
    public const OPCODE_DATA = 0x02;
    public const OPCODE_USR1 = 0x03;
    public const OPCODE_USR2 = 0x04;
    public const OPCODE_USR3 = 0x05;
    public const OPCODE_USR4 = 0x06;
    public const OPCODE_USR5 = 0x07;

    public const OPCODE_CLOSE = 0x08;
    public const OPCODE_PING = 0x09;
    public const OPCODE_PONG = 0x0A;
    public const OPCODE_CTRL1 = 0x0B;
    public const OPCODE_CTRL2 = 0x0C;
    public const OPCODE_CTRL3 = 0x0D;
    public const OPCODE_CTRL4 = 0x0E;
    public const OPCODE_CTRL5 = 0x0F;

    private int $code;

    /**
     * @throws InvalidOpCodeException
     */
    private function __construct(int $code)
    {
        if ($code < self::OPCODE_TEXT || $code > self::OPCODE_CTRL5) {
            throw new InvalidOpCodeException('Invalid opcode value: ' . $code);
        }
        $this->code = $code;
    }

    /**
     * @throws InvalidOpCodeException
     */
    public static function fromInt(int $code): self
    {
        return new self($code);
    }

    public function toInt(): int
    {
        return $this->code;
    }

    public function is(int $code): bool
    {
        return $this->code === $code;
    }

    /**
     * @param string $name
     * @param array{} $parameters
     * @return static
     * @throws InvalidOpCodeException
     */
    public static function __callStatic(string $name, array $parameters): self
    {
        $constant = self::class . '::OPCODE_' . \strtoupper($name);
        if (! \defined($constant)) {
            throw new \BadMethodCallException('Unknown method ' . $name);
        }
        $val = \constant($constant);
        static $cache = [];
        return $cache[$val] ?? $cache[$val] = self::fromInt($val);
    }
}
