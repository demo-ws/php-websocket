<?php

declare(strict_types=1);

namespace PhpSpec\Plugins\Listeners;

use PhpSpec\Event\SpecificationEvent;
use PhpSpec\Loader\Node\ExampleNode;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PhpspecCamelCaseListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            'beforeSpecification' => ['beforeSpecification', -100],
        ];
    }

    public function beforeSpecification(SpecificationEvent $specificationEvent)
    {
        $spec = $specificationEvent->getSpecification();

        foreach ($spec->getClassReflection()->getMethods() as $method) {
            if (preg_match('/^(it|its)[a-zA-Z]+$/', $method->getName()) === 0) {
                continue;
            }

            $spec->addExample(
                new ExampleNode(
                    $this->getName($method->getName()),
                    $method
                )
            );
        }
    }

    protected function getName(string $methodName): string
    {
        $re = '/(?<=[a-z])(?=[A-Z])/x';
        $a = preg_split($re, $methodName);

        $name = implode(' ', $a);
        $name = strtolower($name);
        return ucfirst($name);
    }

}

