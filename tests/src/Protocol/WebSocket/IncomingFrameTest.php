<?php

declare(strict_types=1);

namespace Test\SocketIO\Protocol\WebSocket;

use SocketIO\Protocol\WebSocket\Frame\OpCode;
use SocketIO\Protocol\WebSocket\IncomingFrame;
use PHPUnit\Framework\TestCase;

/**
 * @covers \SocketIO\Protocol\WebSocket\IncomingFrame
 */
class IncomingFrameTest extends TestCase
{
    /**
     * @dataProvider rawFramesProvider
     *
     * @param string $data
     * @param OpCode $opcode
     * @param int $length
     * @param string $payload
     */
    public function testCreatesFromRawData(string $data, OpCode $opcode, int $length, string $payload): void
    {
        $frame = new IncomingFrame($data);
        self::assertEquals($opcode, $frame->header()->opcode());
        self::assertEquals($length, $frame->header()->dataLength());

        $this->canWriteAndReadData($frame, $data, $payload);
    }


    public function canWriteAndReadData(IncomingFrame $frame, string $rawData, string $expectedPayload): void
    {
        $rawData = \substr($rawData, $frame->header()->headerLength());
        $frame->write($rawData);

        self::assertTrue($frame->isFrameFull());
        self::assertSame($expectedPayload, $frame->getPayload());
    }

    /**
     * @return iterable<array{string, OpCode, int, string}>
     */
    public function rawFramesProvider(): iterable
    {
        yield ["\x89\x05Hello", OpCode::PING(), 5, 'Hello'];
    }
}
