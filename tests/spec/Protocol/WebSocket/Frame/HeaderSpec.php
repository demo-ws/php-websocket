<?php

declare(strict_types=1);

namespace spec\SocketIO\Protocol\WebSocket\Frame;

use PhpSpec\ObjectBehavior;
use SocketIO\Protocol\WebSocket\Frame\Header;
use SocketIO\Protocol\WebSocket\Frame\OpCode;

class HeaderSpec extends ObjectBehavior
{
    public function itIsInitializable(): void
    {
        $this->shouldHaveType(Header::class);
    }

    public function itCorrectlyDecodesUnmaskedPingHeader(): void
    {
        $unmaskedData = "\x89\x05\x48\x65\x6c\x6c\x6f";
        $this->beConstructedThrough('fromData', [$unmaskedData]);

        $this->opcode()->shouldBeLike(OpCode::PING());
        $this->isFin()->shouldBe(true);
        $this->reserved1()->shouldBe(false);
        $this->reserved2()->shouldBe(false);
        $this->reserved3()->shouldBe(false);

        $this->dataLength()->shouldBe(5);
        $this->maskingKey()->shouldBeNull();

        $this->toString()->shouldBe("\x89\x05");
    }

    public function itCorrectlyDecodesMaskedPongHeader(): void
    {
        $maskedData = "\x8a\x85\x37\xfa\x21\x3d\x7f\x9f\x4d\x51\x58";
        $this->beConstructedThrough('fromData', [$maskedData]);

        $this->opcode()->shouldBeLike(OpCode::PONG());

        $this->isFin()->shouldBe(true);
        $this->reserved1()->shouldBe(false);
        $this->reserved2()->shouldBe(false);
        $this->reserved3()->shouldBe(false);

        $this->dataLength()->shouldBe(5);
        $this->maskingKey()->shouldNotBeNull();

        $this->toString()->shouldBe("\x8a\x05");
    }

    public function itDecodesAndEncodesLongFrameHeader(): void
    {
        $data = "\x82\x7E\x01\x00";
        $this->beConstructedThrough('fromData', [$data]);

        $this->dataLength()->shouldBe(256);
        $this->toString()->shouldBe($data);
    }

    public function itDecodesAndEncodesVeryLongFrameHeader(): void
    {
        $data = "\x82\x7F\x00\x00\x00\x00\x00\x01\x00\x00";
        $this->beConstructedThrough('fromData', [$data]);

        $this->dataLength()->shouldBe(65536);
        $this->toString()->shouldBe($data);
    }
}
