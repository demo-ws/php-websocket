<?php

declare(strict_types=1);

namespace Test\SocketIO\Protocol\WebSocket\Frame;

use SocketIO\Protocol\WebSocket\Frame\OpCode;
use SocketIO\Protocol\WebSocket\Frame\Header;
use PHPUnit\Framework\TestCase;

/**
 * @covers \SocketIO\Protocol\WebSocket\Frame\Header
 */
class HeaderTest extends TestCase
{
    public function testCorrectlyDecodesUnmaskedPingHeader(): void
    {
        $unmaskedData = "\x89\x05\x48\x65\x6c\x6c\x6f";

        $unmaskedHeader = Header::fromData($unmaskedData);

        self::assertTrue($unmaskedHeader->isFin());
        self::assertFalse($unmaskedHeader->reserved1());
        self::assertFalse($unmaskedHeader->reserved2());
        self::assertFalse($unmaskedHeader->reserved3());

        self::assertTrue($unmaskedHeader->opcode()->is(OpCode::OPCODE_PING));
        self::assertSame(5, $unmaskedHeader->dataLength());
        self::assertNull($unmaskedHeader->maskingKey());

        self::assertSame("\x89\x05", $unmaskedHeader->toString());
    }

    public function testCorrectlyDecodesMaskedPongHeader(): void
    {
        $maskedData = "\x8a\x85\x37\xfa\x21\x3d\x7f\x9f\x4d\x51\x58";

        $maskedHeader = Header::fromData($maskedData);

        self::assertTrue($maskedHeader->isFin());
        self::assertFalse($maskedHeader->reserved1());
        self::assertFalse($maskedHeader->reserved2());
        self::assertFalse($maskedHeader->reserved3());

        self::assertTrue($maskedHeader->opcode()->is(OpCode::OPCODE_PONG));
        self::assertSame(5, $maskedHeader->dataLength());
        self::assertNotNull($maskedHeader->maskingKey());
        self::assertSame("\x8a\x05", $maskedHeader->toString());
    }

    public function testDecodesAndEncodesLongFrameHeader(): void
    {
        $data = "\x82\x7E\x01\x00";
        $header = Header::fromData($data);

        self::assertSame(256, $header->dataLength());
        self::assertSame($data, $header->toString());
    }

    public function testDecodesAndEncodesVeryLongFrameHeader(): void
    {
        $data = "\x82\x7F\x00\x00\x00\x00\x00\x01\x00\x00";
        $header = Header::fromData($data);

        self::assertSame(65536, $header->dataLength());
        self::assertSame($data, $header->toString());
    }
}
