<?php

declare(strict_types=1);

namespace SocketIO\Server\Connection\State;

use SocketIO\Server\Connection\Connection;

interface SateFactoryInterface
{
    /**
     * @param resource $socket
     * @param Connection $connection
     *
     * @return StateInterface
     */
    public function getInitialState($socket, Connection $connection): StateInterface;

    /**
     * @param string $stateCode
     * @phpstan-param StateInterface::STATE_* $stateCode
     * @param resource $socket
     * @param Connection $connection
     *
     * @return StateInterface
     *
     * @throws \InvalidArgumentException
     */
    public function getStateByCode(string $stateCode, $socket, Connection $connection): StateInterface;
}
