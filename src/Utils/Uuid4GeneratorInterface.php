<?php

declare(strict_types=1);

namespace SocketIO\Utils;

interface Uuid4GeneratorInterface
{
    public function getUuid(): string;
}
