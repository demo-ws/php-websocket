<?php

declare(strict_types=1);

namespace SocketIO\Protocol\WebSocket;

use SocketIO\Protocol\WebSocket\Frame\Header;
use SocketIO\Protocol\WebSocket\Frame\OpCode;

class Frame implements FrameInterface
{
    private Header $header;
    private string $data;

    public function __construct(OpCode $opcode, string $data)
    {
        static $headerBuilder = null;
        $headerBuilder ??= \Closure::bind(static function (OpCode $opcode, int $dataLength) {
            $header = new Header();
            $header->opcode = $opcode;
            $header->dataLength = $dataLength;
            $header->headerLength = \strlen($header->toString());
            return $header;
        }, null, Header::class);

        $this->header = $headerBuilder($opcode, \strlen($data));
        $this->data = $data;
    }

    public function header(): Header
    {
        return $this->header;
    }

    public function getPayload(): string
    {
        return $this->data;
    }
}
