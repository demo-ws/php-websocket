<?php

declare(strict_types=1);

namespace SocketIO\Protocol\WebSocket\Frame;

class Header
{
    private bool $isFin = true;
    private bool $reserved1 = false;
    private bool $reserved2 = false;
    private bool $reserved3 = false;

    private OpCode $opcode;
    private int $dataLength = 0;
    private ?string $maskingKey = null;
    private int $headerLength = 0;

    /**
     * @throws InvalidOpCodeException | InvalidHeaderLengthException
     */
    public static function fromData(string $data): self
    {
        if (\strlen($data) < 2) {
            throw new InvalidHeaderLengthException();
        }
        $header = new self();
        [, $char] = \unpack('C', \substr($data, 0, 1));
        $header->isFin = (bool)($char & 1 << 7);
        $header->reserved1 = (bool)($char & 1 << 6);
        $header->reserved2 = (bool)($char & 1 << 5);
        $header->reserved3 = (bool)($char & 1 << 4);
        $header->opcode = OpCode::fromInt($char & 0x0F);

        [, $char] = \unpack('C', \substr($data, 1, 1));
        $mask = $char & 1 << 7;
        $header->dataLength = $char & 0b01111111;

        $maskOffset = 2;
        if ($header->dataLength === 126) {
            if (\strlen($data) < 4) {
                throw new InvalidHeaderLengthException();
            }
            [, $header->dataLength] = \unpack('n', \substr($data, 2, 2));
            $maskOffset += 2;
        } elseif ($header->dataLength === 127) {
            if (\strlen($data) < 10) {
                throw new InvalidHeaderLengthException();
            }
            [, $header->dataLength] = \unpack('J', \substr($data, 2, 8));
            $maskOffset += 8;
        }

        $header->headerLength = $maskOffset;

        if ($mask) {
            if (\strlen($data) < $maskOffset + 4) {
                throw new InvalidHeaderLengthException();
            }
            $header->maskingKey = \substr($data, $maskOffset, 4);
            $header->headerLength += 4;
        }

        return $header;
    }

    public function isFin(): bool
    {
        return $this->isFin;
    }

    public function reserved1(): bool
    {
        return $this->reserved1;
    }

    public function reserved2(): bool
    {
        return $this->reserved2;
    }

    public function reserved3(): bool
    {
        return $this->reserved3;
    }

    public function opcode(): OpCode
    {
        return $this->opcode;
    }

    public function dataLength(): int
    {
        return $this->dataLength;
    }

    public function maskingKey(): ?string
    {
        return $this->maskingKey;
    }

    public function headerLength(): int
    {
        return $this->headerLength;
    }

    public function toString(): string
    {
        $first = $this->opcode->toInt() & 0b00001111;
        if ($this->isFin) {
            $first |= 0b10000000;
        }
        if ($this->reserved1) {
            $first |= 0b01000000;
        }
        if ($this->reserved2) {
            $first |= 0b00100000;
        }
        if ($this->reserved3) {
            $first |= 0b00010000;
        }
        $bytes[] = $first;
        $format = 'CC';
        if ($this->dataLength > 0xFFFF) {
            $bytes[] = 127;
            $format .= 'J';
        } elseif ($this->dataLength > 126) {
            $bytes[] = 126;
            $format .= 'n';
        }
        $bytes[] = $this->dataLength;
        return pack($format, ...$bytes);
    }
}
