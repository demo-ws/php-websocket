<?php

declare(strict_types=1);

namespace SocketIO\Server\Connection;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use SocketIO\Application\ApplicationInterface;
use SocketIO\Server\Connection\State\SateFactoryInterface;
use SocketIO\Server\Connection\State\StateInterface;
use SocketIO\Server\Server;

class Connection implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /** @var LoggerInterface */
    protected $logger;
    private StateInterface $state;
    private Server $server;
    /** @var array<string> */
    private array $buffer = [];
    private Request $request;
    /** @var resource */
    private $socket;
    private bool $disconnected = false;

    private SateFactoryInterface $stateFactory;
    private ApplicationInterface $application;

    /**
     * @param SateFactoryInterface $stateFactory
     * @param Server $server
     * @param resource $socket
     */
    public function __construct(SateFactoryInterface $stateFactory, Server $server, $socket)
    {
        if (! \is_resource($socket)) {
            throw new \InvalidArgumentException('$socket must be a resource!');
        }
        @\stream_set_blocking($socket, false);

        $this->stateFactory = $stateFactory;
        $this->state = $stateFactory->getInitialState($socket, $this);
        $this->server = $server;
        $this->socket = $socket;
        $this->logger = new NullLogger();
    }

    /**
     * @param Request $request
     * @throws HttpRequestException
     */
    public function setRequest(Request $request): void
    {
        $application = $this->server->findApplicationByRequest($request);
        if ($application === null) {
            throw new HttpRequestException(HttpResponseMessage::notFound());
        }

        if (! $application->isAuthorizedRequest($request)) {
            throw new HttpRequestException(HttpResponseMessage::unauthorized());
        }

        $this->application = $application;
        $this->request = $request;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @param string $stateCode
     * @phpstan-param StateInterface::STATE_* $stateCode
     */
    public function setState(string $stateCode): void
    {
        $this->logger->info('New state: ' . $stateCode);
        $this->state = $this->stateFactory->getStateByCode($stateCode, $this->socket, $this);
        $this->state->init();
    }

    public function getCurrentState(): string
    {
        return $this->state->getState();
    }

    public function process(): void
    {
        $this->state->process();
    }

    public function disconnect(): self
    {
        $this->disconnected = true;
        return $this;
    }

    public function isDisconnected(): bool
    {
        return $this->disconnected;
    }

    /**
     * @param string $string
     * @return $this
     * @throws ConnectionException
     */
    public function write(string $string): self
    {
        if ($this->disconnected) {
            $this->logger->warning('Attempt to write data after disconnected!');
            throw new ConnectionException('Cannot write to disconnected connection');
        }

        $this->buffer[] = $string;
        return $this;
    }

    public function hasDataToWrite(): bool
    {
        return $this->buffer !== [];
    }

    /**
     * @throws ConnectionException
     */
    public function flushData(): void
    {
        while (\count($this->buffer)) {
            $data = \array_shift($this->buffer);
            $written = \fwrite($this->socket, $data);
            if ($written === false) {
                $this->logger->warning('Failed to write to socket, looks like socket is closed');
                throw new ConnectionException('Connection is closed');
            }
            if ($written <= $data) {
                if ($written > 0) {
                    $data = \substr($data, $written);
                    \array_unshift($this->buffer, $data);
                }
                break;
            }
        }

        if ($this->canCloseConnection()) {
            $this->closeConnectionAndRemoveSelfFromServerPool();
        }
    }

    private function canCloseConnection(): bool
    {
        return $this->disconnected && $this->buffer === [];
    }

    private function closeConnectionAndRemoveSelfFromServerPool(): void
    {
        fclose($this->socket);
    }
}
