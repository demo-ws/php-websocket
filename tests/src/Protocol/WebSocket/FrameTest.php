<?php

declare(strict_types=1);

namespace Test\SocketIO\Protocol\WebSocket;

use SocketIO\Protocol\WebSocket\Frame;
use PHPUnit\Framework\TestCase;

/**
 * @covers \SocketIO\Protocol\WebSocket\Frame
 */
class FrameTest extends TestCase
{

    public function testFrameCreation(): void
    {
        $data = 'Hello';
        $frame = new Frame(Frame\OpCode::PING(), $data);

        self::assertEquals(Frame\OpCode::PING(), $frame->header()->opcode());
        self::assertEquals(\strlen($data), $frame->header()->dataLength());
        self::assertEquals($data, $frame->getPayload());
    }
}
