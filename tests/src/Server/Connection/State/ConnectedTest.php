<?php

declare(strict_types=1);

namespace Test\SocketIO\Server\Connection\State;

use PHPUnit\Framework\MockObject\MockObject;
use SocketIO\Server\Connection\Connection;
use PHPUnit\Framework\TestCase;
use SocketIO\Server\Connection\State\Connected;
use SocketIO\Server\Connection\State\SateFactoryInterface;
use SocketIO\Server\Connection\State\StateFactory;
use SocketIO\Server\Connection\State\StateInterface;
use SocketIO\Server\Server;
use SocketIO\Utils\Uuid4GeneratorInterface;

/**
 * @covers \SocketIO\Server\Connection\State\Connected
 */
class ConnectedTest extends TestCase
{
    /** @var ?resource */
    private $fh = null;
    /** @var Connection */
    private $connection;
    /** @var Server&MockObject */
    private $server;
    /** @var SateFactoryInterface */
    private $stateFactory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->stateFactory = new StateFactory($this->getUuidGenerator());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        if ($this->fh !== null) {
            fclose($this->fh);
            $this->fh = null;
        }
    }

    public function testPingPongFrames(): void
    {
        $state = $this->getConnectedState('test-fixture://connection/state/connected-ping-pong');
        $state->process();
        $this->connection->flushData();
        self::assertFalse($this->connection->isDisconnected());
        self::assertEquals(StateInterface::STATE_READY, $this->connection->getCurrentState());
    }

    public function testSendCloseAndDisconnectsOnCloseFrame(): void
    {
        $state = $this->getConnectedState('test-fixture://connection/state/connected-close');
        $state->process();
        $this->connection->flushData();
        self::assertTrue($this->connection->isDisconnected());
        self::assertEquals(StateInterface::STATE_ERROR, $this->connection->getCurrentState());
        $this->fh = null;
    }

    private function getConnectedState(string $fixture): Connected
    {
        $this->server = $this->createMock(Server::class);
        $this->fh = fopen($fixture, 'rw');

        $this->connection = new Connection(
            $this->stateFactory,
            $this->server,
            $this->fh
        );
        $this->connection->setState(StateInterface::STATE_READY);

        return new Connected(
            $this->fh,
            $this->connection
        );
    }

    private function getUuidGenerator(): Uuid4GeneratorInterface
    {
        return new class implements Uuid4GeneratorInterface {

            public function getUuid(): string
            {
                return '258EAFA5-E914-47DA-95CA-C5AB0DC85B11';
            }
        };
    }
}
