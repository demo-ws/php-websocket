<?php

declare(strict_types=1);

namespace SocketIO\Server\Connection;

class ConnectionException extends \Exception
{
}
