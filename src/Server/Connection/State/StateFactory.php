<?php

declare(strict_types=1);

namespace SocketIO\Server\Connection\State;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use SocketIO\Server\Connection\Connection;
use SocketIO\Utils\Uuid4GeneratorInterface;

class StateFactory implements SateFactoryInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /** @var LoggerInterface */
    protected $logger;
    private Uuid4GeneratorInterface $uuid4Generator;

    public function __construct(Uuid4GeneratorInterface $uuid4Generator)
    {
        $this->uuid4Generator = $uuid4Generator;
        $this->logger = new NullLogger();
    }

    public function getInitialState($socket, Connection $connection): StateInterface
    {
        return $this->injectLogger(new Handshake($socket, $connection, $this->uuid4Generator));
    }

    public function getStateByCode(string $stateCode, $socket, Connection $connection): StateInterface
    {
        switch ($stateCode) {
            case StateInterface::STATE_READY:
                return $this->injectLogger(new Connected($socket, $connection));
            case StateInterface::STATE_ERROR:
                return $this->injectLogger(new Error($socket, $connection));
            default:
                throw new \InvalidArgumentException('Unknown state code: ' . $stateCode);
        }
    }

    private function injectLogger(StateInterface $state): StateInterface
    {
        if ($state instanceof LoggerAwareInterface) {
            $state->setLogger($this->logger);
        }
        return $state;
    }
}
