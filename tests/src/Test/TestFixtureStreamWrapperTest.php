<?php

declare(strict_types=1);

namespace Test\Test\SocketIO;

use PHPUnit\Framework\TestCase;

class TestFixtureStreamWrapperTest extends TestCase
{
    /**
     * @return resource
     */
    public function testCanOpenFixture()
    {
        $fh = fopen('test-fixture://test', 'r');
        self::assertIsResource($fh);

        return $fh;
    }

    /**
     * @depends testCanOpenFixture
     * @param resource $fh
     */
    public function testCanReadAndWrite($fh)
    {
        self::assertFalse(feof($fh));
        $data = fread($fh, 1024);
        self::assertEquals('this is a test message 1', $data);

        self::assertFalse(feof($fh));
        $data = fread($fh, 1024);
        self::assertEquals('this is a test message 2', $data);

        fwrite($fh, 'this is a test message 3');

        self::assertTrue(feof($fh));
    }
}
