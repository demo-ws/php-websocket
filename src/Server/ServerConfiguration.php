<?php

declare(strict_types=1);

namespace SocketIO\Server;

class ServerConfiguration
{
    private int $port = 80;

    private string $interface = '0.0.0.0';

    public function __construct(
        int $port = null,
        string $interface = null
    ) {
        $this->port = $port ?? $this->port;
        $this->interface = empty($interface) ? $this->interface : $interface;
    }

    public function getPort(): int
    {
        return $this->port;
    }

    public function getInterface(): string
    {
        return $this->interface;
    }
}
