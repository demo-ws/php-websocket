<?php

declare(strict_types=1);

namespace SocketIO\Server\Connection\State;

use SocketIO\Server\Connection\Connection;
use SocketIO\Server\Connection\HttpRequestException;
use SocketIO\Server\Connection\HttpResponseMessage;
use SocketIO\Server\Connection\Request;
use SocketIO\Utils\Uuid4GeneratorInterface;

class Handshake implements StateInterface
{
    private const MAX_HEADER_LENGTH = 8186;
    private const PROTOCOL_VERSION = 13;

    /** @var resource */
    private $socket;
    private Connection $connection;
    private Uuid4GeneratorInterface $uuid4Generator;

    private string $buffer = '';

    /**
     * @param resource $socket
     * @param Connection $connection
     * @param Uuid4GeneratorInterface $uuid4Generator
     */
    public function __construct($socket, Connection $connection, Uuid4GeneratorInterface $uuid4Generator)
    {
        $this->socket = $socket;
        $this->connection = $connection;
        $this->uuid4Generator = $uuid4Generator;
    }

    public function init(): void
    {
    }

    public function process(): void
    {
        $data = \fread($this->socket, 8186);
        if (\is_string($data)) {
            $this->buffer .= $data;
        }

        try {
            $request = $this->tryToParseRequest();
            if ($request === null) {
                return;
            }

            $request->setServerKey($this->calculateServerKet($request->getClientKey()));
            $this->connection->setRequest($request);
            if ($request->getProtocolVersion() !== self::PROTOCOL_VERSION) {
                $this->connection->write((string)HttpResponseMessage::upgradeRequired(self::PROTOCOL_VERSION));
                $this->connection->setState(self::STATE_ERROR);
            } else {
                $this->connection->write(
                    (string)HttpResponseMessage::switchingProtocol($request)
                );
                $this->connection->setState(self::STATE_READY);
            }
        } catch (HttpRequestException $exception) {
            $this->connection->write((string)$exception->getHttpMessage());
            $this->connection->setState(self::STATE_ERROR);
        } catch (\Throwable $exception) {
            $this->connection->write((string)HttpResponseMessage::serverError($exception));
            $this->connection->setState(self::STATE_ERROR);
        }
    }

    public function getState(): string
    {
        return self::STATE_HANDSHAKE;
    }

    /**
     * @throws HandshakeException|HttpRequestException
     */
    private function tryToParseRequest(): ?Request
    {
        $this->buffer = \str_replace("\r\n", "\n", $this->buffer);
        $this->buffer = \str_replace("\r", "\n", $this->buffer);

        $lastBytes = \substr($this->buffer, -2, 2);

        if (\strlen($this->buffer) >= self::MAX_HEADER_LENGTH) {
            throw new HandshakeException('Header is too long! Looks like wrong protocol!');
        }

        if ($lastBytes !== "\n\n") {
            return null;
        }

        $lines = \explode("\n", \rtrim($this->buffer));
        if (count($lines) < 3) {
            throw new HandshakeException('Malformed header!');
        }

        $request = array_shift($lines);
        [$method, $url] = $this->parseRequest($request);
        $headers = $this->parseHeaders($lines);
        return new Request($method, $url, $headers);
    }

    /**
     * @param string$requestLine
     *
     * @return array{string, string}
     * @throws HandshakeException
     */
    private function parseRequest(string $requestLine): array
    {
        $parts = \explode(' ', trim($requestLine));
        if (\count($parts) !== 3) {
            throw new HandshakeException('Failed to parse request line');
        }

        $httpVersion = \explode('/', \strtoupper($parts[2]));
        if (\count($httpVersion) !== 2) {
            throw new HandshakeException('Failed to parse request line');
        }
        if ($httpVersion[0] !== 'HTTP') {
            throw new HandshakeException('Failed to parse request line');
        }
        if (version_compare('1.1', $httpVersion[1]) > 0) {
            throw new HandshakeException('Failed to parse request line');
        }

        $method = \strtoupper($parts[0]);
        $url = $parts[1];

        return [$method, $url];
    }

    /**
     * @param array<string> $lines
     *
     * @return array<string, string>
     * @throws HandshakeException
     */
    private function parseHeaders(array $lines): array
    {
        $result = [];
        foreach ($lines as $line) {
            $parts = \explode(':', $line, 2);
            if (\count($parts) < 2) {
                throw new HandshakeException('Failed to parse headers');
            }
            $key = \strtolower(\trim($parts[0]));
            $value = \trim($parts[1]);
            $result[$key] = $value;
        }

        return $result;
    }

    private function calculateServerKet(string $clientKet): string
    {
        return \base64_encode(\sha1($clientKet . $this->uuid4Generator->getUuid(), true));
    }
}
