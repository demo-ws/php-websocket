<?php

declare(strict_types=1);

namespace SocketIO\Application;

use SocketIO\Server\Connection\Request;

interface ApplicationInterface
{
    public function canHandleRequest(Request $request): bool;

    public function isAuthorizedRequest(Request $request): bool;

    public function isDefault(): bool;
}
