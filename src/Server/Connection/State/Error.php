<?php

declare(strict_types=1);

namespace SocketIO\Server\Connection\State;

use SocketIO\Server\Connection\Connection;

class Error implements StateInterface
{
    /** @var resource */
    private $socket;
    private Connection $connection;

    /**
     * @param resource $socket
     * @param Connection $connection
     */
    public function __construct($socket, Connection $connection)
    {
        $this->socket = $socket;
        $this->connection = $connection;
    }

    public function process(): void
    {
    }

    public function init(): void
    {
        $this->connection->disconnect();
    }

    public function getState(): string
    {
        return self::STATE_ERROR;
    }
}
