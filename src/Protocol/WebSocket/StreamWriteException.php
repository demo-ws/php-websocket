<?php

declare(strict_types=1);

namespace SocketIO\Protocol\WebSocket;

class StreamWriteException extends FrameException
{
}
