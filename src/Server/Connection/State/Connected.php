<?php

declare(strict_types=1);

namespace SocketIO\Server\Connection\State;

use SocketIO\Protocol\WebSocket\Frame;
use SocketIO\Protocol\WebSocket\FrameProcessorInterface;
use SocketIO\Protocol\WebSocket\IncomingFrame;
use SocketIO\Protocol\WebSocket\FrameDataWriterInterface;
use SocketIO\Protocol\WebSocket\FrameException;
use SocketIO\Protocol\WebSocket\FrameInterface;
use SocketIO\Protocol\WebSocket\StreamWriteException;
use SocketIO\Server\Connection\Connection;

class Connected implements StateInterface, FrameProcessorInterface
{
    /** @var resource */
    private $socket;
    private Connection $connection;
    private string $buffer = '';
    private ?IncomingFrame $currentFrame = null;

    /**
     * @param resource $socket
     * @param Connection $connection
     */
    public function __construct($socket, Connection $connection)
    {
        $this->socket = $socket;
        $this->connection = $connection;
    }

    public function process(): void
    {
        do {
            $data = \fread($this->socket, 8186);
            $this->buffer .= $data;
        } while ($data !== false && \strlen($data) === 8186);

        while (\strlen($this->buffer) > 0) {
            $this->processBuffer();
        }
    }

    public function init(): void
    {
    }

    public function getState(): string
    {
        return self::STATE_READY;
    }

    private function processBuffer(): void
    {
        try {
            // php-stan magick for impure method calls
            $currentFrame = $this->currentFrame;
            if ($currentFrame !== null) {
                $this->fillFrameWithData($currentFrame);
                if ($currentFrame->isFrameFull()) {
                    $this->processFrame($currentFrame);
                    $this->currentFrame = null;
                }
            } else {
                $this->currentFrame = new IncomingFrame($this->buffer);
                $this->buffer = substr($this->buffer, $this->currentFrame->header()->headerLength());
            }
        } catch (FrameException $exception) {
        }
    }

    /**
     * @phpstan-pure
     */
    private function fillFrameWithData(FrameDataWriterInterface $dataWriter): void
    {
        try {
            $dataWritten = $dataWriter->write($this->buffer);
            if ($dataWritten) {
                $this->buffer = \substr($this->buffer, $dataWritten);
            }
        } catch (StreamWriteException $exception) {
        }
    }

    private function processFrame(IncomingFrame $frame): void
    {
        $frame->processWith($this);
    }

    public function onCloseFrame(FrameInterface $frame): void
    {
        $this->sendFrame($frame);
        $this->connection->setState(self::STATE_ERROR);
    }

    public function onPingFrame(FrameInterface $frame): void
    {
        $this->sendFrame(new Frame(Frame\OpCode::PONG(), $frame->getPayload()));
    }

    public function onPongFrame(FrameInterface $frame): void
    {
    }

    public function onDataFrame(FrameInterface $frame): void
    {
    }

    public function onTextFrame(FrameInterface $frame): void
    {
    }

    public function onCustomFrame(FrameInterface $frame): void
    {
    }

    public function onCustomCtrlFrame(FrameInterface $frame): void
    {
    }

    private function sendFrame(FrameInterface $frame): void
    {
        $this->connection->write($frame->header()->toString() . $frame->getPayload());
    }
}
