<?php

declare(strict_types=1);

namespace SocketIO\Protocol\WebSocket;

interface FrameDataWriterInterface
{
    /**
     * Возвращает кол-во записанных данных
     *
     * @throws StreamWriteException
     */
    public function write(string $data): int;

    public function isFrameFull(): bool;
}
