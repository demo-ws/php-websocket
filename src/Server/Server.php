<?php

declare(strict_types=1);

namespace SocketIO\Server;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use SocketIO\Application\ApplicationInterface;
use SocketIO\Server\Connection\Connection;
use SocketIO\Server\Connection\ConnectionFactory;
use SocketIO\Server\Connection\Request;

class Server implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /** @var LoggerInterface */
    protected $logger;
    private ServerConfiguration $configuration;
    private ConnectionFactory $connectionFactory;
    /** @var array<ApplicationInterface> */
    private array $applications;
    private ?ApplicationInterface $defaultApplication;

    /** @var resource */
    private $serverSocket = null;
    /** @var array<Connection>  */
    private array $connections = [];
    /** @var array<resource>  */
    private array $socketPool = [];

    private bool $running = false;

    /**
     * @param ServerConfiguration $configuration
     * @param ConnectionFactory $connectionFactory
     * @param iterable<ApplicationInterface> $applications
     */
    public function __construct(
        ServerConfiguration $configuration,
        ConnectionFactory $connectionFactory,
        iterable $applications = []
    ) {
        $this->configuration = $configuration;
        $this->connectionFactory = $connectionFactory;
        $this->logger = new NullLogger();

        foreach ($applications as $application) {
            $this->registerApplication($application);
        }
    }

    public function registerApplication(ApplicationInterface $application): void
    {
        $this->applications[] = $application;
        if ($application->isDefault()) {
            $this->defaultApplication = $application;
        }
    }

    public function run(): void
    {
        $this->logger->info('Starting server');
        $this->listen();
        $this->logger->info('Started');
        $this->running = true;
        while ($this->running) {
            $readSockets = $this->getSocketsForReading();
            $writeSockets = $this->getSocketsForWriting();
            $exceptSockets = null;

            $readySockets = @\stream_select($readSockets, $writeSockets, $exceptSockets, 1, 100000);
            if ($readySockets === false) {
                $this->onSocketSelectError();
                continue;
            }

            foreach ($readSockets as $readSocket) {
                if ($readSocket === $this->serverSocket) {
                    $this->tryToAcceptNewConnection();
                } else {
                    $this->processConnectionInput($readSocket);
                }
            }

            foreach ($writeSockets as $writeSocket) {
                $this->processConnectionOutput($writeSocket);
            }
        }
    }

    private function listen(): void
    {
        $socket = \stream_socket_server(sprintf(
            'tcp://%s:%d',
            $this->configuration->getInterface(),
            $this->configuration->getPort()
        ), $errorCode, $errorMessage);

        if ($socket === false) {
            $this->throwException(new \RuntimeException(
                'Failed to create socket: ' . $errorMessage,
                $errorCode
            ));
        }

        if (! \stream_set_blocking($socket, false)) {
            $this->throwException(new \RuntimeException(
                'Failed to set non-blocking mode'
            ));
        }

        $this->serverSocket = $socket;
        $this->socketPool[$this->getSocketId($socket)] = $socket;
    }

    /**
     * @return array<resource>
     */
    private function getSocketsForReading(): array
    {
        return $this->socketPool;
    }

    /**
     * @return array<resource>
     */
    private function getSocketsForWriting(): array
    {
        $result = [];
        foreach ($this->connections as $socketId => $connection) {
            if ($connection->hasDataToWrite()) {
                $result[$socketId] = $this->socketPool[$socketId];
            }
        }

        return $result;
    }

    public function stop(): void
    {
        $this->running = false;
    }

    public function findApplicationByRequest(Request $request): ?ApplicationInterface
    {
        foreach ($this->applications as $application) {
            if ($application->canHandleRequest($request)) {
                return $application;
            }
        }
        if ($this->defaultApplication !== null) {
            return $this->defaultApplication;
        }

        return null;
    }

    private function tryToAcceptNewConnection(): void
    {
        $newConnection = @\stream_socket_accept($this->serverSocket);
        if ($newConnection === false) {
            $this->onSocketAcceptError();
            return;
        }

        stream_set_blocking($newConnection, false);
        $socketId = $this->getSocketId($newConnection);
        $this->socketPool[$socketId] = $newConnection;
        $this->connections[$socketId] = $this->connectionFactory->getConnection($newConnection, $this);
        $this->logger->info('New incoming connection #' . $socketId, ['socket' => $socketId]);
    }

    /**
     * @param resource $socket
     *
     * @return int
     */
    private function getSocketId($socket): int
    {
        return (int)$socket;
    }

    /**
     * @param \Throwable $throwable
     *
     * @return never-return
     * @throws \Throwable
     */
    private function throwException(\Throwable $throwable): void
    {
        $this->logger->error($throwable->getMessage(), ['exception' => $throwable]);
        throw $throwable;
    }

    private function onSocketSelectError(): void
    {
        $this->logger->warning('Failed to accomplish select operation');
    }

    private function onSocketAcceptError(): void
    {
        $this->logger->warning('Failed to accept new connection');
    }

    /**
     * @param resource $socket
     */
    private function getConnectionBySocket($socket): Connection
    {
        $socketId = $this->getSocketId($socket);
        $connection = $this->connections[$socketId] ?? null;
        if ($connection === null) {
            throw new \OutOfBoundsException('Failed to get Connection for socket id #' . $socketId);
        }

        return $connection;
    }

    /**
     * @param resource $socket
     */
    private function processConnectionInput($socket): void
    {
        $this->getConnectionBySocket($socket)->process();
    }

    /**
     * @param resource $socket
     */
    private function processConnectionOutput($socket): void
    {
        $this->getConnectionBySocket($socket)->flushData();
    }
}
