<?php

declare(strict_types=1);

namespace PhpSpec\Plugins;

use PhpSpec\Extension;
use PhpSpec\ServiceContainer;
use PhpSpec\Plugins\Listeners\PhpspecCamelCaseListener;

class PhpspecCamelCase implements Extension
{
    public function load(ServiceContainer $container, array $params): void
    {
        $container->define(
            'event_dispatcher.listeners.annotation',
            function () {
                return new PhpspecCamelCaseListener();
            },
            ['event_dispatcher.listeners']
        );
    }
}
