<?php

declare(strict_types=1);

namespace %namespace%;

%imports%

/**
 * @covers \%subject%
 */
final class %name% extends ObjectBehavior
{
    public function itIsInitializable(): void
    {
        $this->shouldHaveType(%subject_class%::class);
    }
}
