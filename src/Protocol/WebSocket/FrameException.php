<?php

declare(strict_types=1);

namespace SocketIO\Protocol\WebSocket;

class FrameException extends \Exception
{
}
