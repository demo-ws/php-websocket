<?php

declare(strict_types=1);

namespace SocketIO\Protocol\WebSocket;

use SocketIO\Protocol\WebSocket\Frame\Header;
use SocketIO\Protocol\WebSocket\Frame\OpCode;

class IncomingFrame implements FrameInterface, FrameDataWriterInterface
{
    private Header $header;
    /** @var resource */
    private $fh;
    private int $dataWritten = 0;

    /**
     * @throws Frame\InvalidOpCodeException | Frame\InvalidHeaderLengthException
     * @throws StreamCreationException
     */
    public function __construct(string $rawHeader)
    {
        $this->header = Header::fromData($rawHeader);
        if (($fh = \fopen('php://temp', 'w+b')) === false) {
            throw new StreamCreationException('Failed to create stream for writing data');
        }
        $this->fh = $fh;
    }

    public function header(): Header
    {
        return $this->header;
    }

    public function getPayload(): string
    {
        \rewind($this->fh);
        return \fread($this->fh, $this->dataWritten) ?: '';
    }

    public function write(string $data): int
    {
        $dataToWrite = \min(\strlen($data), $this->header->dataLength() - $this->dataWritten);
        $dataWritten = 0;
        if ($dataToWrite > 0) {
            $dataWritten = \fwrite($this->fh, $data, $dataToWrite);
            if ($dataWritten === false) {
                throw new StreamWriteException('Failed to write data to temp stream');
            }
            $this->dataWritten += $dataWritten;
        }
        return $dataWritten;
    }

    public function isFrameFull(): bool
    {
        return $this->dataWritten >= $this->header->dataLength();
    }

    public function processWith(FrameProcessorInterface $frameProcessor): void
    {
        switch ($this->header->opcode()) {
            case OpCode::TEXT():
                $frameProcessor->onTextFrame($this);
                break;
            case OpCode::DATA():
                $frameProcessor->onDataFrame($this);
                break;
            case OpCode::CLOSE():
                $frameProcessor->onCloseFrame($this);
                break;
            case OpCode::PING():
                $frameProcessor->onPingFrame($this);
                break;
            case OpCode::PONG():
                $frameProcessor->onPongFrame($this);
                break;
            case OpCode::USR1():
            case OpCode::USR2():
            case OpCode::USR3():
            case OpCode::USR4():
            case OpCode::USR5():
                $frameProcessor->onCustomFrame($this);
                break;
            case OpCode::CTRL1():
            case OpCode::CTRL2():
            case OpCode::CTRL3():
            case OpCode::CTRL4():
            case OpCode::CTRL5():
                $frameProcessor->onCustomCtrlFrame($this);
                break;
        }
    }

    public function __destruct()
    {
        \fclose($this->fh);
    }
}
