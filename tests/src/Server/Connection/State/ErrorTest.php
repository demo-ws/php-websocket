<?php

declare(strict_types=1);

namespace Test\SocketIO\Server\Connection\State;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use SocketIO\Application\ApplicationInterface;
use SocketIO\Server\Connection\Connection;
use SocketIO\Server\Connection\State\Handshake;
use SocketIO\Server\Connection\State\SateFactoryInterface;
use SocketIO\Server\Connection\State\StateFactory;
use SocketIO\Server\Connection\State\StateInterface;
use SocketIO\Server\Server;
use SocketIO\Utils\Uuid4GeneratorInterface;

/**
 * @covers \SocketIO\Server\Connection\State\Handshake
 */
class ErrorTest extends TestCase
{
    /** @var ?resource */
    private $fh = null;
    /** @var Connection */
    private $connection;
    /** @var Server&MockObject */
    private $server;
    /** @var SateFactoryInterface */
    private $stateFactory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->stateFactory = new StateFactory($this->getUuidGenerator());
        $this->server = $this->createMock(Server::class);
        $fh = fopen('test-fixture://connection/state/error', 'rw');
        self::assertNotFalse($fh);
        $this->fh = $fh;

        $this->connection = new Connection(
            $this->stateFactory,
            $this->server,
            $this->fh
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        if ($this->fh !== null) {
            fclose($this->fh);
            $this->fh = null;
        }
    }


    public function testErrorStateClosesConnection(): void
    {
        self::assertFalse($this->connection->isDisconnected());
        self::assertNotEquals(StateInterface::STATE_ERROR, $this->connection->getCurrentState());
        $this->connection->setState(StateInterface::STATE_ERROR);
        self::assertTrue($this->connection->isDisconnected());
        self::assertEquals(StateInterface::STATE_ERROR, $this->connection->getCurrentState());
    }


    private function getUuidGenerator(): Uuid4GeneratorInterface
    {
        return new class implements Uuid4GeneratorInterface {

            public function getUuid(): string
            {
                return '258EAFA5-E914-47DA-95CA-C5AB0DC85B11';
            }
        };
    }
}
