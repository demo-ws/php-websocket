<?php

declare(strict_types=1);

namespace SocketIO\Server\Connection;

class Request
{
    private string $path;
    /** @var array<string, mixed> */
    private array $queryParams = [];

    private string $host;
    private string $clientKey;
    private string $serverKey;
    private int $clientProtocolVersion;
    private ?string $origin = null;
    /** @var array<string>  */
    private array $protocols = [];
    /** @var array<string>  */
    private array $extensions = [];

    /** @var array<string, string> */
    private array $headers;

    /**
     * @param string $method
     * @param string $url
     * @param array<string, string> $headers
     *
     * @throws HttpRequestException
     */
    public function __construct(string $method, string $url, array $headers)
    {
        if (\strtoupper($method) !== 'GET') {
            throw new HttpRequestException(HttpResponseMessage::badRequest());
        }

        $urlParts = \parse_url($url);
        if ($urlParts === false) {
            throw new HttpRequestException(HttpResponseMessage::badRequest());
        }

        $path = $urlParts['path'] ?? '';
        if ($path === '' || \substr($path, 0, 1) !== '/') {
            throw new HttpRequestException(HttpResponseMessage::badRequest());
        }

        $this->path = $path;

        if (! empty($urlParts['query'])) {
            \parse_str($urlParts['query'], $this->queryParams);
        }

        $this->headers = $headers = $this->normalizeHeaderKeys($headers);

        $host = $headers['host'] ?? null;
        if (!\is_string($host) || empty($host)) {
            throw new HttpRequestException(HttpResponseMessage::badRequest());
        }
        $this->host = \strtolower($host);

        if (\strtolower($headers['upgrade'] ?? '') !== 'websocket') {
            throw new HttpRequestException(HttpResponseMessage::badRequest());
        }

        $connectionHeader = \array_filter(\array_map(
            static fn ($val) => \strtolower(\trim($val)),
            \explode(',', $headers['connection'] ?? '')
        ));

        if (!\in_array('upgrade', $connectionHeader)) {
            throw new HttpRequestException(HttpResponseMessage::badRequest());
        }

        $key = (string)($headers['sec-websocket-key'] ?? '');
        if (! $this->validClientKey($key)) {
            throw new HttpRequestException(HttpResponseMessage::badRequest());
        }
        $this->clientKey = $key;

        $version = (int)($headers['sec-websocket-version'] ?? '');
        if ($version !== 13) {
            throw new HttpRequestException(HttpResponseMessage::badRequest());
        }
        $this->clientProtocolVersion = $version;

        $origin = $headers['origin'] ?? null;
        $this->origin = empty($origin) ? null : (string)$origin;

        $protocols = (string)($headers['sec-websocket-protocol'] ?? '');
        if ($protocols !== '') {
            $protocols = preg_split('/,\s*/', $protocols);
            if ($protocols === false) {
                throw new HttpRequestException(HttpResponseMessage::badRequest());
            }
            $this->protocols = $protocols;
        }

        $extensions = (string)($headers['sec-websocket-extensions'] ?? '');
        if ($extensions !== '') {
            $extensions = preg_split('/,\s*/', $extensions);
            if ($extensions === false) {
                throw new HttpRequestException(HttpResponseMessage::badRequest());
            }
            $this->extensions = $extensions;
        }
    }

    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return array<string, mixed>
     */
    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getClientKey(): string
    {
        return $this->clientKey;
    }

    public function getServerKey(): string
    {
        return $this->serverKey;
    }

    public function getProtocolVersion(): int
    {
        return $this->clientProtocolVersion;
    }

    /**
     * @return array<string, string>
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    /**
     * @return array<string>
     */
    public function getProtocols(): array
    {
        return $this->protocols;
    }

    /**
     * @return array<string>
     */
    public function getExtensions(): array
    {
        return $this->extensions;
    }

    public function setServerKey(string $serverKey): void
    {
        $this->serverKey = $serverKey;
    }

    /**
     * @param array<string, mixed> $headers
     * @return array<string, mixed>
     */
    private function normalizeHeaderKeys(array $headers): array
    {
        $result = [];
        foreach ($headers as $key => $value) {
            $result[\strtolower($key)] = $value;
        }

        return $result;
    }

    private function validClientKey(string $key): bool
    {
        $decoded = \base64_decode($key);
        return (!empty($decoded) && \strlen($decoded) === 16);
    }
}
