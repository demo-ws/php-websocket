<?php

declare(strict_types=1);

namespace SocketIO\Server\Connection\State;

use SocketIO\Server\Connection\ConnectionException;

class HandshakeException extends ConnectionException
{
}
