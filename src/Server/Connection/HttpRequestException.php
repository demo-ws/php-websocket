<?php

declare(strict_types=1);

namespace SocketIO\Server\Connection;

use Throwable;

class HttpRequestException extends ConnectionException
{
    private HttpResponseMessage $httpMessage;

    public function __construct(HttpResponseMessage $httpMessage, Throwable $previous = null)
    {
        $this->httpMessage = $httpMessage;
        parent::__construct($httpMessage->getMessage(), $httpMessage->getCode(), $previous);
    }

    public function getHttpMessage(): HttpResponseMessage
    {
        return $this->httpMessage;
    }
}
