<?php

declare(strict_types=1);

namespace Test\SocketIO;

use function PHPUnit\Framework\assertEquals;

class TestFixtureStreamWrapper
{
    private static bool $registered = false;

    /** @var resource */
    public $context;
    private ?\Generator $generator = null;
    private int $expectedWriteCalls = 0;
    private int $actualWriteCalls = 0;

    public static function register()
    {
        if (self::$registered === false) {
            $res = \stream_wrapper_register('test-fixture', self::class);
            self::$registered = true;
        }
    }

    public function __construct()
    {
    }

    public function __destruct()
    {
    }

    public function dir_closedir(): bool
    {
        throw new \Error('Work with directories is not supported');
    }

    public function dir_opendir(string $path, int $options): bool
    {
        throw new \Error('Work with directories is not supported');
    }

    public function dir_readdir(): string
    {
        throw new \Error('Work with directories is not supported');
    }

    public function dir_rewinddir(): bool
    {
        throw new \Error('Work with directories is not supported');
    }

    public function mkdir(string $path, int $mode, int $options): bool
    {
        throw new \Error('Work with directories is not supported');
    }

    public function rename(string $path_from, string $path_to): bool
    {
        throw new \Error('Rename is not supported');
    }

    public function rmdir(string $path, int $options): bool
    {
        throw new \Error('Work with directories is not supported');
    }

    public function stream_cast(int $cast_as): bool
    {
        return false;
    }

    public function stream_close(): void
    {
        $this->generator = null;
        assertEquals(
            $this->expectedWriteCalls,
            $this->actualWriteCalls,
            'There were ' . $this->expectedWriteCalls . ' expected writes to stream.'
        );
    }

    public function stream_eof(): bool
    {
        return $this->generator === null || $this->generator->valid() === false;
    }

    public function stream_flush(): bool
    {
        return false;
    }

    public function stream_lock(int $operation): bool
    {
        return false;
    }

    public function stream_metadata(string $path, int $option, mixed $value): bool
    {
        return false;
    }

    public function stream_open(
        string $path,
        string $mode,
        int $options,
        ?string &$opened_path
    ): bool {
        $path = \str_replace('test-fixture://', '', $path);
        $fixturesPath = \realpath(__DIR__ . '/../fixtures') . '/';
        $path = $fixturesPath . $path . '.json';

        $showErrors = $options & STREAM_REPORT_ERRORS;

        if (! \file_exists($path)) {
            $showErrors && \trigger_error('Fixture "' . $path . '" does not exists!', E_USER_WARNING);
            return false;
        }

        if (! \is_file($path)) {
            $showErrors && \trigger_error('Fixture "' . $path . '" is not a file!', E_USER_WARNING);
            return false;
        }

        if (! \is_readable($path)) {
            $showErrors && \trigger_error('Fixture "' . $path . '" is not readable!', E_USER_WARNING);
            return false;
        }

        $data = @\file_get_contents($path);
        try {
            $data = @\json_decode($data, true, JSON_THROW_ON_ERROR);
        } catch (\JsonException $exception) {
            $showErrors && \trigger_error('Fixture "' . $path . '" is not a valid JSON!', E_USER_WARNING);
            return false;
        }

        $this->generator = $this->getGenerator($data);

        foreach ($data as $datum) {
            if (($datum[1] ?? null) !== null) {
                $this->expectedWriteCalls++;
            }
        }

        return true;
    }

    private function getGenerator(array $data): \Generator
    {
        foreach ($data as $datum) {
            $toSend = $datum[0] ?? null;
            $toExpect = $datum[1] ?? null;
            $isBase64 = $datum[2] ?? false;

            if ($toSend !== null) {
                yield $isBase64 ? \base64_decode($toSend) : $toSend;
            }

            if ($toExpect !== null) {
                $get = yield;
                assertEquals($isBase64 ? \base64_decode($toExpect) : $toExpect, $get);
            }
        }
    }

    public function stream_read(int $count): string
    {
        if ($this->generator !== null && $this->generator->valid()) {
            $val = $this->generator->current();
            $this->generator->next();
            return $val;
        }
        return '';
    }

    public function stream_seek(int $offset, int $whence = SEEK_SET): bool
    {
        return false;
    }

    public function stream_set_option(int $option, int $arg1, ?int $arg2): bool
    {
        return false;
    }

    public function stream_stat(): bool
    {
        return false;
    }

    public function stream_tell(): int
    {
        return 0;
    }

    public function stream_truncate(int $new_size): bool
    {
        return false;
    }

    public function stream_write(string $data): int
    {
        if ($this->generator) {
            $this->actualWriteCalls++;

            $this->generator->send($data);
            return \strlen($data);
        }

        return 0;
    }

    public function unlink(string $path): bool
    {
        return false;
    }

    public function url_stat(string $path, int $flags): bool
    {
        return false;
    }
}
