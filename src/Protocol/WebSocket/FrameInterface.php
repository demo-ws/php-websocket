<?php

declare(strict_types=1);

namespace SocketIO\Protocol\WebSocket;

use SocketIO\Protocol\WebSocket\Frame\Header;

interface FrameInterface
{
    public function header(): Header;

    public function getPayload(): string;
}
