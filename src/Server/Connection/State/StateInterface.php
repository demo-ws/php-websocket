<?php

declare(strict_types=1);

namespace SocketIO\Server\Connection\State;

interface StateInterface
{
    public const STATE_HANDSHAKE = 'handshake';
    public const STATE_READY = 'ready';
    public const STATE_ERROR = 'error';

    public function process(): void;

    public function init(): void;

    public function getState(): string;
}
