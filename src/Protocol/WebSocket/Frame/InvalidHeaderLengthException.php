<?php

declare(strict_types=1);

namespace SocketIO\Protocol\WebSocket\Frame;

use SocketIO\Protocol\WebSocket\FrameException;

class InvalidHeaderLengthException extends FrameException
{
}
