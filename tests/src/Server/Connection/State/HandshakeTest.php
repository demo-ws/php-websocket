<?php

declare(strict_types=1);

namespace Test\SocketIO\Server\Connection\State;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use SocketIO\Application\ApplicationInterface;
use SocketIO\Server\Connection\Connection;
use SocketIO\Server\Connection\State\Handshake;
use SocketIO\Server\Connection\State\SateFactoryInterface;
use SocketIO\Server\Connection\State\StateFactory;
use SocketIO\Server\Connection\State\StateInterface;
use SocketIO\Server\Server;
use SocketIO\Utils\Uuid4GeneratorInterface;

/**
 * @covers \SocketIO\Server\Connection\State\Handshake
 */
class HandshakeTest extends TestCase
{
    /** @var ?resource */
    private $fh = null;
    /** @var Connection */
    private $connection;
    /** @var Server&MockObject */
    private $server;
    /** @var SateFactoryInterface */
    private $stateFactory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->stateFactory = new StateFactory($this->getUuidGenerator());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        if ($this->fh !== null) {
            fclose($this->fh);
            $this->fh = null;
        }
    }


    public function testHandshakeSwitchesStateAndSends101ResponseIfEverythingOk(): void
    {
        $handshakeState = $this->getHandshakeState('test-fixture://connection/state/handshake-ok');
        $this->server->expects($this->once())
            ->method('findApplicationByRequest')
            ->willReturn($this->configureSuitableApplication());

        $handshakeState->process();
        $this->connection->flushData();
        self::assertFalse($this->connection->isDisconnected());
        self::assertEquals(StateInterface::STATE_READY, $this->connection->getCurrentState());
    }

    public function testHandshakeSwitchesStateAndSends404ResponseIfApplicationNotFound(): void
    {
        $handshakeState = $this->getHandshakeState('test-fixture://connection/state/handshake-404');
        $this->server->expects($this->once())
            ->method('findApplicationByRequest')
            ->willReturn(null);

        $handshakeState->process();
        $this->connection->flushData();
        self::assertTrue($this->connection->isDisconnected());
        self::assertEquals(StateInterface::STATE_ERROR, $this->connection->getCurrentState());
        $this->fh = null;
    }

    public function testHandshakeSwitchesStateAndSends403ResponseIfApplicationNotAuthorizingRequest(): void
    {
        $handshakeState = $this->getHandshakeState('test-fixture://connection/state/handshake-403');
        $this->server->expects($this->once())
            ->method('findApplicationByRequest')
            ->willReturn($this->configureUnauthorizedApplication());

        $handshakeState->process();
        $this->connection->flushData();
        self::assertTrue($this->connection->isDisconnected());
        self::assertEquals(StateInterface::STATE_ERROR, $this->connection->getCurrentState());
        $this->fh = null;
    }

    private function getHandshakeState(string $fixture): StateInterface
    {
        $this->server = $this->createMock(Server::class);
        $this->fh = fopen($fixture, 'rw');

        $this->connection = new Connection(
            $this->stateFactory,
            $this->server,
            $this->fh
        );

        return new Handshake(
            $this->fh,
            $this->connection,
            $this->getUuidGenerator()
        );
    }

    private function getUuidGenerator(): Uuid4GeneratorInterface
    {
        return new class implements Uuid4GeneratorInterface {

            public function getUuid(): string
            {
                return '258EAFA5-E914-47DA-95CA-C5AB0DC85B11';
            }
        };
    }

    private function configureSuitableApplication(): ApplicationInterface
    {
        $application = $this->createMock(ApplicationInterface::class);
        $application->method('canHandleRequest')->willReturn(true);
        $application->method('isAuthorizedRequest')->willReturn(true);

        return $application;
    }

    private function configureUnauthorizedApplication(): ApplicationInterface
    {
        $application = $this->createMock(ApplicationInterface::class);
        $application->method('canHandleRequest')->willReturn(true);
        $application->method('isAuthorizedRequest')->willReturn(false);

        return $application;
    }
}
