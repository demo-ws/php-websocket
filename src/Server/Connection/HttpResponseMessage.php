<?php

declare(strict_types=1);

namespace SocketIO\Server\Connection;

class HttpResponseMessage
{
    private int $code;
    private string $message;
    /** @var array<string, string|array<int|string>> */
    private array $headers = [];

    private function __construct(int $code, string $message)
    {
        $this->code = $code;
        $this->message = $message;
    }

    public static function switchingProtocol(Request $request): self
    {
        $message = new self(101, 'Switching Protocols');
        $message->headers = [
            'Upgrade' => 'websocket',
            'Connection' => 'Upgrade',
            'Sec-WebSocket-Accept' => $request->getServerKey(),
        ];

        if ($request->getOrigin() !== null) {
            $message->headers['Access-Control-Allow-Origin'] = $request->getOrigin();
        }

        return $message;
    }

    /**
     * @param array<int|numeric-string>|null $supportedVersions
     * @return self
     */
    public static function badRequest(?array $supportedVersions = null): self
    {
        $message = new self(400, 'Bad Request');
        if ($supportedVersions) {
            $message->headers = [
                'Sec-WebSocket-Version' => implode(', ', $supportedVersions)
            ];
        }

        return $message;
    }

    public static function upgradeRequired(int $supportedProtocolVersion): self
    {
        $message = new self(426, 'Upgrade Required');
        if ($supportedProtocolVersion) {
            $message->headers = [
                'Sec-WebSocket-Version' => (string)$supportedProtocolVersion
            ];
        }

        return $message;
    }

    public static function unauthorized(): self
    {
        $message = new self(403, 'Forbidden');

        return $message;
    }

    public static function notFound(): self
    {
        $message = new self(404, 'Not Found');

        return $message;
    }

    public static function unsupportedMethod(): self
    {
        $message = new self(405, 'Method Not Allowed');

        return $message;
    }

    public static function serverError(\Throwable $exception): self
    {
        $message = new self(505, 'Internal Server Error');

        return $message;
    }

    public function getCode(): int
    {
        return $this->code;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function __toString(): string
    {
        $result = "HTTP/1.1 $this->code $this->message\r\n";
        foreach ($this->headers as $header => $value) {
            if (\is_array($value)) {
                $value = implode(', ', $value);
            }
            $result .= "$header: $value\r\n";
        }

        $result .= "\r\n";
        return $result;
    }
}
