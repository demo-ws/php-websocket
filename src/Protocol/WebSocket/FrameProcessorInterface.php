<?php

declare(strict_types=1);

namespace SocketIO\Protocol\WebSocket;

use SocketIO\Protocol\WebSocket\FrameInterface;

interface FrameProcessorInterface
{
    public function onCloseFrame(FrameInterface $frame): void;
    public function onPingFrame(FrameInterface $frame): void;
    public function onPongFrame(FrameInterface $frame): void;
    public function onDataFrame(FrameInterface $frame): void;
    public function onTextFrame(FrameInterface $frame): void;

    public function onCustomFrame(FrameInterface $frame): void;
    public function onCustomCtrlFrame(FrameInterface $frame): void;
}
